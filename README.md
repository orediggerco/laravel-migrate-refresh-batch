# Laravel Migrate Refresh Batch

### An artisan command to rerun the last batch of migrations

Laravel has a `migrate:refresh` command to rollback and rerun every migration in your application. It also supports specifying a specific number of migrations to rollback with its `step` option, which takes an integer.

This package adds a command to refresh the last batch of migrations, without having to count how many to use the `step` option.

## Installation

You can install this package via composer:
```bash
composer require orediggerco/laravel-migrate-refresh-batch
```

Or, since this package may be most useful to you during development, you can add it only as a development dependency via composer as well:
```bash
composer require-dev orediggerco/laravel-migrate-refresh-batch
```

Next add the `Orediggerco\MigrateRefreshBatch\Commands\MigrateRefreshBatch` class to your console kernel:
```php
// app/Console/Kernel.app
protected $commands = [
    ...
    \Orediggerco\MigrateRefreshBatch\Commands\MigrateRefreshBatch::class,
]
```

If you are only using this command as a development resource, there are many ways to conditionally include the Command in the console kernel based on the current application environment.

## Usage

This command will rollback and re-run the last batch of migrations in your applciation:
```bash
php artisan migrate:refresh-batch
```

All options available to the core `migrate:refresh` command, with the exception of the `step` option, are avaialble with this command as well. For example:
```bash
php artisan migrate:refresh-batch --force --seed
```

The `step` option is not added to this command since it has no meaning in the context of rolling back a batch of migrations.

## License

The MIT License (MIT). Please see the [License File](LICENSE.md) for more information.
