<?php namespace Orediggerco\MigrateRefreshBatch\Commands;

use Illuminate\Database\Console\Migrations\RefreshCommand;

class MigrateRefreshBatch extends RefreshCommand
{
    /**
    * The console command name.
    *
    * @var string
    */
    protected $name = 'migrate:refresh-batch';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Rollback and rerun the last batch of migrations';

    /**
    * Execute the console command
    *
    * @return void
    */
    public function fire()
    {
        if (! $this->confirmToProceed()) {
            return;
        }

        $database = $this->input->getOption('database');

        $force = $this->input->getOption('force');

        $path = $this->input->getOption('path');

        $this->call(
            'migrate:rollback',
            array(
                '--database' => $database,
                '--force' => $force,
                '--path' => $path,
            )
        );

        $this->call(
            'migrate',
            array(
                '--database' => $database,
                '--force' => $force,
                '--path' => $path,
            )
        );

        if ($this->needsSeeding()) {
            $this->runSeeder($database);
        }
    }

    /**
    * Get the console command options.
    *
    * @return array
    */
    protected function getOptions()
    {
        return array_filter(
            parent::getOptions(),
            function ($value) {
                return $value[0] != 'step';
            }
        );
    }
}
